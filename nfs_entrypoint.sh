#!/usr/bin/env bash

set -euo pipefail

rpcbind
exec inotifywait --monitor "$nfs_mount_directory"
