#!/usr/bin/env bash

set -euo pipefail

delimiter=":"

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <import-1> [ <import-2> [ <import-3> [...] ] ]"
   echo
   echo "Arguments:"
   echo "  import-N: <hostname>$delimiter<source>$delimiter<destination>"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" == 0 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

tcp_port=2049
exports="${@}"

for x in "${exports[@]}"; do
   num_delimiters="$(
      echo "$x" |
      tr --delete --complement "$delimiter" |
      wc --chars
   )"

   if [ "$num_delimiters" != 2 ]; then
      usage
      exit 1
   fi

   host="$(echo "$x" | awk -F "$delimiter" '{ print $1 }')"
   src="$(echo "$x" | awk -F "$delimiter" '{ print $2 }')"
   target="$(echo "$x" | awk -F "$delimiter" '{ print $3 }')"

   if [ -z "$host" ] || [ -z "$src" ] || [ -z "$target" ]; then
      usage
      exit 1
   fi

   full_target="$nfs_mount_directory/$target"

   mkdir --parents "$full_target"

   mount --types nfs \
         --options nolock,proto=tcp,port="$tcp_port" \
         "$host:$src" \
         "$full_target"
done
