FROM debian:jessie
ENV DEBIAN_FRONTEND noninteractive

# Variables ####################################################################
ENV nfs_entrypoint "/usr/local/bin/entrypoint"
ENV nfs_add_mount "/usr/local/bin/add_mount"
ENV nfs_mount_directory "/mnt/nfs"

# Packages #####################################################################
RUN apt-get update --quiet --quiet \
 && apt-get install --quiet --quiet --assume-yes \
    inotify-tools \
    nfs-common \
 && rm --recursive --force /var/lib/apt/lists/*

# NFS ##########################################################################
COPY nfs_entrypoint.sh "${nfs_entrypoint}"
COPY nfs_add_mount.sh "${nfs_add_mount}"
RUN chmod +x "${nfs_entrypoint}"
RUN chmod +x "${nfs_add_mount}"

RUN mkdir --parents "${nfs_mount_directory}"
VOLUME "${nfs_mount_directory}"

# This should be the $nfs_entrypoint variable, but we can't use the
# shell-interpreted version of ENTRYPOINT for signal-handling reasons.
ENTRYPOINT ["/usr/local/bin/entrypoint"]
