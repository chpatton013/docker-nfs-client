# Docker NFS Client

Use this container to mount directories exported over NFS.

## Usage

```
docker build --tag=chpatton013/docker-nfs-client .
docker run --privileged --rm --name=nfs-client chpatton013/docker-nfs-client
docker exec nfs-client <list of imported directories>
```

The directory import list is composed of entries with the following syntax:

```
<hostname>:<export-path>:<name>
```

Every entry in this list is mounted under `/mnt/nfs`. For example, if the
container is started with the parameter `nfs-hostname:/exports/foo:bar`
then the directory is mounted at `/mnt/nfs/bar`.

The `/mnt/nfs` directory is exported as a volume by this container. This allows
you to easily forward those mounted directories onto other containers with
`--volumes-from`.
